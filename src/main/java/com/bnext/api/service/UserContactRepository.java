package com.bnext.api.service;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bnext.api.model.UserContact;

public interface UserContactRepository extends JpaRepository<UserContact, Integer>{

	List<UserContact> findContactById(int id);

}
