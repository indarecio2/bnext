package com.bnext.api.service;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bnext.api.model.User;

public interface UserRepository extends JpaRepository<User, Integer>{

	List<User> findContactById(int id);

}
