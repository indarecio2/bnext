package com.bnext.api.service;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bnext.api.model.Contact;

public interface ContactRepository extends JpaRepository<Contact, Integer>{

	List<Contact> findContactById(int id);

}
