package com.bnext.api.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "user")
@Data
public class User {
	
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column
	private String username;
	@Column
	private String name;
	@Column
	private String surnames;
	@Column
	private String email;
	@Column
	private String password;
	@Column
	private String age;
	@Column
	private String active;
	@Column
	private Date lastLogging;
	@Column
	private Date creationDate;
	

}
