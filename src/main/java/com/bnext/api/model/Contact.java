package com.bnext.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "contact")
@Data
public class Contact {
	
	@Id
	private int number;
	@Column
	private String name;
	@Column
	private String surnames;
	@Column
	private String email;
}
