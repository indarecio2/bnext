package com.bnext.api.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bnext.api.service.ContactRepository;
import com.bnext.api.service.UserContactRepository;
import com.bnext.api.service.UserRepository;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import com.bnext.api.model.Contact;
import com.bnext.api.model.User;
import com.bnext.api.model.UserContact;

@RestController
public class UserController {

	@Autowired
	private UserContactRepository repository;
	
	@Autowired
	private UserRepository repositoryUser;
	
	@Autowired
	private ContactRepository repositoryContact;

	private List<Integer> contactid3;
	
	@PostMapping("/saveUser")
	@ApiResponses({ @ApiResponse(code = 200, message = "OK"), @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "BAD REQUEST")})
	public String saveUser(@RequestBody User user) {
		repositoryUser.save(user);
		return "User save";
	}
	
	@PostMapping("/createdContact")
	@ApiResponses({ @ApiResponse(code = 200, message = "OK"), @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "BAD REQUEST")})
	public String saveContact(@RequestBody Contact contact) {
		repositoryContact.save(contact);
		return "Contact save";
	}
	
	@PostMapping("/saveContactAgent")
	@ApiResponses({ @ApiResponse(code = 200, message = "OK"), @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "BAD REQUEST")})
	public String saveContact(@RequestBody UserContact userContact) {
		repository.save(userContact);
		return "Contact save in agent";
	}
	
	@GetMapping("/getAllContact")
	@ApiResponses({ @ApiResponse(code = 200, message = "OK"), @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "BAD REQUEST")})
	public List<Contact> getAll(){
		return repositoryContact.findAll();
	}
	
	@GetMapping("/getContact/{id}")
	@ApiResponses({ @ApiResponse(code = 200, message = "OK"), @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "BAD REQUEST")})
	public List<UserContact> getfindContactById(@PathVariable int id){
		return repository.findContactById(id);
	}
	
	@GetMapping("/getContactComun/")
	@ApiResponses({ @ApiResponse(code = 200, message = "OK"), @ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message = "BAD REQUEST")})
	public List<Integer> getfindContactComun(int id1, int id2){
		List<UserContact> contactid1 = repository.findContactById(id1);
		List<UserContact> contactid2 = repository.findContactById(id2);
		contactid3 = null;
		
		for (UserContact userContact : contactid1) {
			for (UserContact userContact2 : contactid2) {
				if(userContact.getNumber() == userContact2.getNumber()) {
					contactid3.add(userContact.getNumber());
				}
			}
		}
		return contactid3;
	}
	
}